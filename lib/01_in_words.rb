class Fixnum
  def in_words
    num = self
    trillion = 1_000_000_000_000
    billion = 1_000_000_000
    million = 1_000_000
    thousand = 1_000

    words = []
    words << "zero" if num == 0

    while num > 0
      case
      when num >= trillion
        words << read_hundreds(num/trillion)
        if num/trillion%100 >= 10 && num%100 <= 19
          words << read_teens(num/trillion%100)
        elsif num/trillion%100 >= 20
          words << read_tens(num/trillion%100)
          words << read_ones(num/trillion%10)
        elsif num/trillion%100 < 10
          words << read_ones(num/trillion%10)
        end
        words << "trillion"
        num = num % trillion
      when num >= billion
        words << read_hundreds(num/billion)
        if num/billion%100 >= 10 && num%100 <= 19
          words << read_teens(num/billion%100)
        elsif num/billion%100 >= 20
          words << read_tens(num/billion%100)
          words << read_ones(num/billion%10)
        elsif num/billion%100 < 10
          words << read_ones(num/billion%10)
        end
        words << "billion"
        num = num % billion
      when num >= million
        words << read_hundreds(num/million)
        if num/million%100 >= 10 && num%100 <= 19
          words << read_teens(num/million%100)
        elsif num/million%100 >= 20
          words << read_tens(num/million%100)
          words << read_ones(num/million%10)
        elsif num/million%100 < 10
          words << read_ones(num/million%10)
        end
        words << "million"
        num = num % million
      when num >= thousand
        words << read_hundreds(num/thousand)
        if num/thousand%100 >= 10 && num%100 <= 19
          words << read_teens(num/thousand%100)
        elsif num/thousand%100 >= 20
          words << read_tens(num/thousand%100)
          words << read_ones(num/thousand%10)
        elsif num/thousand%100 < 10
          words << read_ones(num/thousand%10)
        end
        words << "thousand"
        num = num % thousand
      when num >= 100
        words << read_hundreds(num)
        if num%100 >= 10 && num%100 <= 19
          words << read_teens(num%100)
        elsif num%100 >= 20
          words << read_tens(num%100)
          words << read_ones(num%10)
        end
        num = 0
      when num >= 20
        words << read_tens(num)
        words << read_ones(num%10)
        num = 0
      when num >= 10
        words << read_teens(num)
        num = 0
      when num > 0
        words << read_ones(num)
        num = 0
      end
    end
    words.delete("")
    words.join(" ")
  end

  def read_ones(num)
    case num
    when 1
      "one"
    when 2
      "two"
    when 3
      "three"
    when 4
      "four"
    when 5
      "five"
    when 6
      "six"
    when 7
      "seven"
    when 8
      "eight"
    when 9
      "nine"
    else
      ""
    end
  end

  def read_teens(num)
    case num
    when 10
      "ten"
    when 11
      "eleven"
    when 12
      "twelve"
    when 13
      "thirteen"
    when 14
      "fourteen"
    when 15
      "fifteen"
    when 16
      "sixteen"
    when 17
      "seventeen"
    when 18
      "eighteen"
    when 19
      "nineteen"
    else
      ""
    end
  end

  def read_tens(num)
    if num >= 20 && num <= 29
      "twenty"
    elsif num >= 30 && num <= 39
      "thirty"
    elsif num >= 40 && num <= 49
      "forty"
    elsif num >= 50 && num <= 59
      "fifty"
    elsif num >= 60 && num <= 69
      "sixty"
    elsif num >= 70 && num <= 79
      "seventy"
    elsif num >= 80 && num <= 89
      "eighty"
    elsif num >= 90 && num <= 99
      "ninety"
    else
      ""
    end
  end

  def read_hundreds(num)
    if num >= 100 && num <= 199
      "one hundred"
    elsif num >= 200 && num <= 299
      "two hundred"
    elsif num >= 300 && num <= 399
      "three hundred"
    elsif num >= 400 && num <= 499
      "four hundred"
    elsif num >= 500 && num <= 599
      "five hundred"
    elsif num >= 600 && num <= 699
      "six hundred"
    elsif num >= 700 && num <= 799
      "seven hundred"
    elsif num >= 800 && num <= 899
      "eight hundred"
    elsif num >= 900 && num <= 999
      "nine hundred"
    else
      ""
    end
  end
end
